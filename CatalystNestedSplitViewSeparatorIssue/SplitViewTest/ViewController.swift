//
//  ViewController.swift
//  SplitViewTest
//
//  Created by Adrian Schönig on 14.12.20.
//

import UIKit

class OuterSplitViewController: UISplitViewController {

  override func awakeFromNib() {
    super.awakeFromNib()
    
    preferredDisplayMode = .oneBesideSecondary
    preferredSplitBehavior = .tile
    primaryBackgroundStyle = .sidebar
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    
    print("Hello")
    // Do any additional setup after loading the view.
  }


}

