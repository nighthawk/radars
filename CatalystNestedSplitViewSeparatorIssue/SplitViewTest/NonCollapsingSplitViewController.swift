//
//  NonCollapsingSplitViewController.swift
//  SplitViewTest
//
//  Created by Adrian Schönig on 12/8/2022.
//

import UIKit

class NonCollapsingSplitViewController: UISplitViewController {
  
  var allowCollapsing: Bool = false
  
  @objc func toggleSidebar(_ sender: Any) {}

  override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
    if action == #selector(toggleSidebar) {
      return allowCollapsing // Don't intercept that toggle
    } else {
      return super.canPerformAction(action, withSender: sender)
    }
  }
}
