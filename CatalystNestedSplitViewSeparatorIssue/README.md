# Catalyst: Items preceding sidebar tracking seperator misplaced

- Category: Mac Catalyst
- Latest test: 12 August 2022. macOS 13.0 beta 5; Xcode 14 beta 5
- Feedback: FB11223405
- Status: Open

When using a sidebar in Mac Catalyst in an nested split view controller along with the `.primarySidebarTrackingSeparatorItemIdentifier`, adding a toolbar item in front of the sidebar tracking separator lead to those toolbars items misplaces, as well as the sidebar toggle and the document title.

Steps to reproduce:

1. Run the sample on a Mac

Expected result:

- The plus button is to the left of the sidebar separator (on top of the first split's primary view)
- The sidebar toggle and document title are to the right of the sidebar separator (on top of the second split's view)

Actual result:

- The plus button is to the right of the sidebar separator (on to pof the first split's primary view)
- The sidebar toggle and document title are on top of the second split's secondary view

Regression:

Not using `.primarySidebarTrackingSeparatorItemIdentifier` works as expected.

---

#### Expected

<img src="1_expected_placement.png" width=627/>

#### Actual

<img src="2_actual_placement.png" width=627/>