# Sidebar tracking separator ducks under window's traffic light

- Category: Mac Catalyst
- Latest test: 7 June 2021. macOS 11.4; Xcode 12.5
- Feedback: FB9129426
- Status: Open

Summary:

When using a sidebar in Mac Catalyst along with the `.primarySidebarTrackingSeparatorItemIdentifier`, collapsing the sidebar messes up the toolbar moving the sidebar button and title behind the window's traffic light buttons.

Steps to reproduce:

1. Run the sample on Mac
2. Press the sidebar button

Expected:

Title and sidebar button are next to traffic light buttons.

Actual:

Title and sidebar button duck under traffic light buttons. (And an exception is thrown related to the touch bar.)

Regression:

Not using `.primarySidebarTrackingSeparatorItemIdentifier` works as expected.

---

#### Initial

<img src="1_before.png" width=627/>

#### Expected

<img src="2_expected.png" width=627/>

#### Actual

<img src="3_actual.png" width=627/>