//
//  CustomCatalystMenuButtonApp.swift
//  CustomCatalystMenuButton
//
//  Created by Adrian Schönig on 9/6/2022.
//

import SwiftUI

@main
struct CustomCatalystMenuButtonApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
