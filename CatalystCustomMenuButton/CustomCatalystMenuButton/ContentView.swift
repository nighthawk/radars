//
//  ContentView.swift
//  CustomCatalystMenuButton
//
//  Created by Adrian Schönig on 9/6/2022.
//

import SwiftUI

struct ContentView: View {
  @State var formula: String = "rating == 5"
  
  @ViewBuilder
  private var menuOptions: some View {
    Button("Clear") {
      formula = ""
    }
    .disabled(formula.isEmpty)
    
    Divider()
    
    Button("Copy") {
      UIPasteboard.general.string = formula
    }
    .disabled(formula.isEmpty)
    
    Button("Paste") {
      formula = UIPasteboard.general.string ?? ""
    }
    .disabled(UIPasteboard.general.string != nil)
  }
  
  var body: some View {
    Form {
      HStack {
        Button {
          print("Button tapped")
        } label: {
          Image(systemName: "wand.and.stars")
            .resizable()
            .renderingMode(.template)
            .aspectRatio(contentMode: .fit)
            .frame(width: 15)
            .padding(EdgeInsets(top: 10, leading: 10, bottom: 10, trailing: 0))
          
          Text(formula.isEmpty ? "Add" : formula)
            .lineLimit(1)
            .font(.system(.footnote, design: .monospaced))
            .padding(EdgeInsets(top: 5, leading: 0, bottom: 5, trailing: 10))

          Spacer()
        }
        .buttonStyle(BorderlessButtonStyle())
        .contextMenu {
          menuOptions
        }
        
        Divider()
        
        SwiftUI.Menu {
          menuOptions
        } label: {
          Label {
            Text(" ")
          } icon: {
            Image(systemName: "chevron.down")
          }
        }
        .menuStyle(BorderlessButtonMenuStyle())
      }
    }
  }
}

struct ContentView_Previews: PreviewProvider {
  static var previews: some View {
    ContentView()
      .previewLayout(.fixed(width: 300, height: 100))
  }
}
