# Catalyst: Borderless Menu button not getting borderless style anymore

- Category: Mac Catalyst
- Latest test: 12 August 2022. macOS 13.0 beta 5; Xcode 14 beta 5
- Feedback: FB10120466
- Status: Open

If you use a SwiftUI Menu in Catalyst with the "Optimized for Mac" paradigm, borderless menu button styles are no longer applied in macOS 13. That worked fine in macOS 12. The issue can both be seen in apps built with Xcode 13 and also when re-compiled with Xcode 14 for macOS 13.

The attached screen recordings show the previous look and behaviour from macOS 12 and also when running the same app build on macOS 13.

Attached is also a simplified code sample that demonstrates the issue. The menu looks like a regular menu button, rather than a tappable image that behaves like a menu button. Note that it works as expected when compiling Catalyst in "Scaled to Match iPad" mode; the regression is in the "Optimize for Mac" mode.
