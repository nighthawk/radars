//
//  SwiftUIBindingSegFaultApp.swift
//  SwiftUIBindingSegFault
//
//  Created by Adrian Schönig on 22/6/21.
//

import SwiftUI

@main
struct SwiftUIBindingSegFaultApp: App {
  @State var schemas: [SchemaBuild] = []
  @State var selected: SchemaBuild? = nil
  
  var body: some Scene {
    WindowGroup {
      FieldList(schemas: $schemas, selected: $selected) { schema in
        Text(schema.wrappedValue.title)
      }
    }
  }
}
