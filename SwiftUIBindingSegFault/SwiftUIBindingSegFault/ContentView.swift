//
//  ContentView.swift
//  SwiftUIBindingSegFault
//
//  Created by Adrian Schönig on 22/6/21.
//

import SwiftUI

struct SchemaBuild: Identifiable, Hashable {
  var id: String { key }
  
  var title: String
  var key: String
}

struct FieldList<Destination>: View where Destination: View {
  @Binding var schemas: [SchemaBuild]
  @Binding var selected: SchemaBuild?
  var allowInsert: Bool = false
  var destination: ((Binding<SchemaBuild>) -> Destination)?

  var body: some View {
    List {
      ForEach($schemas) { $schema in
        Text(schema.title)
      }
    }
  }
}

#if DEBUG

struct FieldList_Previews: PreviewProvider {
  fileprivate class Wrapper {
    @State var schemas: [SchemaBuild] = []
    @State var selected: SchemaBuild? = nil
  }

  fileprivate static let wrapper = Wrapper()

  static var previews: some View {
    Group {
      FieldList(schemas: wrapper.$schemas, selected: wrapper.$selected, allowInsert: true) { Text("\($0.wrappedValue.title)") }
        .previewLayout(.fixed(width: 320, height: 200))

      FieldList(schemas: wrapper.$schemas, selected: wrapper.$selected, allowInsert: true) { Text("\($0.wrappedValue.title)") }
        .previewLayout(.fixed(width: 320, height: 200))
        .colorScheme(.dark)

      FieldList<Never>(schemas: wrapper.$schemas, selected: wrapper.$selected)
        .previewLayout(.fixed(width: 320, height: 200))

      FieldList<Never>(schemas: wrapper.$schemas, selected: wrapper.$selected)
        .previewLayout(.fixed(width: 320, height: 200))
        .colorScheme(.dark)
    }
  }
}

#endif
