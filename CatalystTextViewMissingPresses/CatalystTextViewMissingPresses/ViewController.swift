//
//  ViewController.swift
//  CatalystTextViewMissingPresses
//
//  Created by Adrian Schönig on 4/7/21.
//

import UIKit

class ViewController: UIViewController {
  
  @IBOutlet weak var textView: InterceptingTextView!
  @IBOutlet weak var label: UILabel!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    // Do any additional setup after loading the view.
    
    label.text = ""
    textView.interceptor = intercept
  }

  func intercept(_ keys: [UIKeyboardHIDUsage]) -> Bool {
    guard let first = keys.first else { return false }
    
    switch first {
    case .keyboardTab:
      label.text = (label.text ?? "") + "Tab\n"
      return true
      
    case .keyboardReturnOrEnter:
      label.text = (label.text ?? "") + "Enter\n"
      return true
      
    default:
      return false

    }
    
  }

}

