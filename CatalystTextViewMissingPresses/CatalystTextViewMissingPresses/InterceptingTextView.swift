//
//  InterceptingTextView.swift
//  CatalystTextViewMissingPresses
//
//  Created by Adrian Schönig on 4/7/21.
//

import UIKit

class InterceptingTextView: UITextView {
  
  var interceptor: ([UIKeyboardHIDUsage]) -> Bool = { _ in false }

  override func pressesBegan(_ presses: Set<UIPress>, with event: UIPressesEvent?) {
    let codes = presses.compactMap(\.key?.keyCode)
    if interceptor(codes) {
      return
    }
    super.pressesBegan(presses, with: event)
  }
  
}
